package chat.chocolate.hello_world;

import chat.chocolate.hello_world.interfaces.ListDAO;
import chat.chocolate.hello_world.resources.ListResource;
import io.dropwizard.Application;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.skife.jdbi.v2.DBI;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

public class HelloWorld extends Application<HelloWorldConfig> {
    public static void main(String[] args) throws Exception {
        new HelloWorld().run(args);
    }

    @Override
    public void initialize(Bootstrap<HelloWorldConfig> bootstrap) {

    }

    @Override
    public void run(HelloWorldConfig configuration, Environment environment) {
        // Enable CORS headers
        final FilterRegistration.Dynamic cors =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        cors.setInitParameter("allowedOrigins", "*");
        cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin");
        cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");

        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");

        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "mysql");
        final ListDAO dao = jdbi.onDemand(ListDAO.class);
        environment.jersey().register(new ListResource(dao));
    }
}
