package chat.chocolate.hello_world.resources;

import chat.chocolate.hello_world.interfaces.ListDAO;
import chat.chocolate.hello_world.models.ToDoList;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.xml.ws.Response;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Path("api/v1/list/")
public class ListResource {
    ListDAO dao;
    public ListResource(ListDAO dao) {
        this.dao = dao;
    }

    @GET
    public String getToDoLists() throws JsonProcessingException {
        ToDoList l1 = new ToDoList("List 1", "description of List 1");
        ToDoList l2 = new ToDoList(this.dao.findNameById(1), "testtest");
        List<ToDoList> list = new ArrayList();
        Collections.addAll(list, l1, l2);

        return new ObjectMapper().writeValueAsString(list);
    }

    @POST
    @Path("new")
    public Response createList(ToDoList list) {

        return new ObjectMapper().writeValueAsString();
    }
}
