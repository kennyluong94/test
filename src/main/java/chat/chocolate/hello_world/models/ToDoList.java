package chat.chocolate.hello_world.models;

import com.fasterxml.jackson.annotation.JsonGetter;

public class ToDoList {
    String name;
    String description;
    public ToDoList(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @JsonGetter
    public String getName() {
        return this.name;
    }

    @JsonGetter
    public String getDescription() {
        return this.description;
    }
}
