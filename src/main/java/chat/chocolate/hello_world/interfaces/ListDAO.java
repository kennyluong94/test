package chat.chocolate.hello_world.interfaces;

import chat.chocolate.hello_world.models.ToDoList;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;

import java.util.List;

public interface ListDAO {
    @SqlUpdate("insert into lists (name, description) values (:name, :description)")
    void insert(@Bind("name") String name, @Bind("description") String description);

    @SqlQuery("SELECT name FROM todolist.lists WHERE id = :id")
    String findNameById(@Bind("id") Integer id);

    @SqlQuery("SELECT description todolist.lists WHERE id = :id")
    String findDescriptionById(@Bind("id") Integer id);

    void close();
}
